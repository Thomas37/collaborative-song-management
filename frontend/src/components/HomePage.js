import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import ButtonGroup from "@material-ui/core/ButtonGroup"
import CreateRoomPage from "./CreateRoomPage";
import RoomJoinPage from "./RoomJoinPage";
import RoomPage from "./RoomPage";
import AddSongPage from "./AddSongPage";

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roomCode: null,
        }
        this.clearRoomCode = this.clearRoomCode.bind(this)
    }

    async componentDidMount() {
        fetch('/api/user-in-room')
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    roomCode: data.code,
                })
            })
        this.clearRoomCode = this.clearRoomCode.bind(this)
    }

    renderHomePage() {
        return (
            <Grid container spacing={3} className="center">
                <Grid item xs={12} align="center">
                    <Typography variant="h3" component="h3">
                        House party
                    </Typography>
                </Grid>
                <Grid item xs={12} align="center">
                    <ButtonGroup variant="contained" color="primary" >
                        <Button color="primary" to="/join" component={ Link }>
                            Join a room
                        </Button>

                        <Button color="secondary" to="/create" component={ Link }>
                            Create a room
                        </Button>
                    </ButtonGroup>
                </Grid>
            </Grid>
        );
    }

    clearRoomCode() {
        this.setState({
                    roomCode: null,
                })
    }

    render() {
        return(
        <Router>
            <Switch>
                <Route exact path='/' render={() => {
                    return this.state.roomCode ? <Redirect to={`/room/${this.state.roomCode}`}/> : this.renderHomePage()
                }}/>
                <Route path='/add-song' component={AddSongPage}/>
                <Route path='/join' component={RoomJoinPage}/>
                <Route path='/create' component={CreateRoomPage}/>

                <Route path='/update' render={(props) => {
                    return <CreateRoomPage {...props} update={true}/>
                }}/>

                <Route path='/room/:roomCode' render={(props) => {
                    return <RoomPage {...props} leaveRoomCallback={this.clearRoomCode}/>
                }}/>
            </Switch>
        </Router>
        );
    }


}