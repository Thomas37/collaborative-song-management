import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import Typography from "@material-ui/core/Typography"
import {LinearProgress, Snackbar} from "@material-ui/core";
import {green} from "@material-ui/core/colors";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import {Alert} from "@material-ui/lab";

export default class SearchedSong extends Component {
    constructor(props) {
        super(props);
        this.state = {added: false}
        this.addSongToQueue = this.addSongToQueue.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    addSongToQueue() {

        const options = {
            method: 'POST',
            headers: { "Content-Type": "application/json"},
        }
        fetch("/spotify/add-song?uri=" + this.props.uri + "&title=" + this.props.name + "&artist=" + this.props.artist_string + "&picture=" + this.props.album_cover, options).then(() => {
            this.setState({added: true})})
    }

    handleClose() {
        this.setState({added: false})
    }

    render() {
        let toast;

        if (this.state.added) {
            toast = <Snackbar open={open} autoHideDuration={2000} onClose={this.handleClose} style={{position: "fixed", bottom: "1em"}}>
                          <Alert severity="success">
                            Song added !
                          </Alert>
                    </Snackbar>
        }

        return (
            <Card>
                <Grid container alignContent="center">
                    <Grid item align="center" xs={4}>
                        <img src={ this.props.album_cover } height="100%" width="100%"/>
                    </Grid>
                    <Grid item align="center" xs={8} className="dark-component">
                        <Typography component="h5" variant="h5" style={{margin: '1em 0 0.2em 0'}}>
                            {this.props.name}
                        </Typography>
                        <Typography variant="subtitle1">
                            {this.props.artist_string}
                        </Typography>
                        <Button style={{ color: green[500] }} onClick={this.addSongToQueue}>
                            <AddCircleIcon />
                            Add to queue
                        </Button>
                        {toast}
                    </Grid>
                </Grid>
                <LinearProgress variant="determinate" value={100}/>
            </Card>
        )
    }
}