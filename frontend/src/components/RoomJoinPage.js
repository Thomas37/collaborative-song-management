import React, { Component } from "react";
import { Link } from "react-router-dom"
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import TextField from "@material-ui/core/TextField"

export default class RoomJoinPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roomCode: "",
            error: "",
            pseudo: ""
        }

        this._handleTextFieldChange = this._handleTextFieldChange.bind(this);
        this._handlePseudoTextFieldChange = this._handlePseudoTextFieldChange.bind(this);
        this._roomButtonPressed = this._roomButtonPressed.bind(this);

    }



    render() {
        return (
            <Grid container spacing={2} alignContent="center" alignItems="center" className="center">
                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>
                <Grid item xs={12} lg={10} xl={8} align="center">
                    <Typography variant="h4" component="h4">
                        Join a room
                    </Typography>
                </Grid>
                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>


                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>
                <Grid item xs={12} lg={10} xl={8} align="center">
                    <TextField
                        error= { this.state.error }
                        label="Code"
                        placeholder="Enter a room code"
                        value={ this.state.roomCode }
                        helperText={ this.state.error }
                        variant="outlined"
                        onChange={ this._handleTextFieldChange }
                        fullWidth="100%"
                    />
                </Grid>
                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>


                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>
                <Grid item xs={12} lg={10} xl={8} align="center">
                    <TextField
                        error= { this.state.error }
                        label="Pseudo"
                        placeholder="Enter a pseudo"
                        value={ this.state.pseudo }
                        helperText={ this.state.error }
                        variant="outlined"
                        onChange={ this._handlePseudoTextFieldChange }
                        fullWidth="100%"
                    />
                </Grid>
                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>


                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>
                <Grid item xs={12} lg={10} xl={8}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} lg xl align="center">
                            <Button variant="contained" color="secondary" to="/" component={Link} fullWidth="100%">Back</Button>
                        </Grid>
                        <Grid item xs={6} lg xl align="center">
                            <Button variant="contained" color="primary" onClick={ this._roomButtonPressed } fullWidth="100%">Enter room</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={0} lg={1} xl={2} align="center">
                </Grid>

            </Grid>
        )
    }

    _handleTextFieldChange(e) {
        this.setState({
            roomCode: e.target.value,
        })
    }

    _handlePseudoTextFieldChange(e) {
        this.setState({
            pseudo: e.target.value,
        })
    }

    _roomButtonPressed() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                code: this.state.roomCode,
                pseudo: this.state.pseudo,
            })
        }

        fetch('/api/join-room', requestOptions)
            .then((response) => {
                if (response.ok) {
                    this.props.history.push(`/room/${this.state.roomCode}`)
                }
                else {
                    this.setState({
                        error: "Room not found"
                    })
                }
            })
            .catch((error) => console.log(error))
    }
}