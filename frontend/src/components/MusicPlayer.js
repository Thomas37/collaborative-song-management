import React, { Component } from "react";
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import IconButton from "@material-ui/core/IconButton"
import Typography from "@material-ui/core/Typography"
import SkipNextIcon from "@material-ui/icons/SkipNext"
import PlayArrowIcon from "@material-ui/icons/PlayArrow"
import PauseIcon from "@material-ui/icons/Pause"
import {LinearProgress} from "@material-ui/core";

export default class MusicPlayer extends Component {
    constructor(props) {
        super(props);

    }

    playSong() {
        const options = {
            method: 'PUT',
            headers: { "Content-Type": "application/json"},
        }
        fetch("/spotify/play", options);
    }

    pauseSong() {
        const options = {
            method: 'PUT',
            headers: { "Content-Type": "application/json"},
        }
        fetch("/spotify/pause", options);
    }

    skipSong() {
        const options = {
            method: 'POST',
            headers: { "Content-Type": "application/json"},
        }
        fetch("/spotify/skip", options);
    }


    render() {
        const songProgress = (this.props.time / this.props.duration) * 100

        const items = []

        if (this.props.votes !== undefined && this.props.votes.length > 0) {
            items.push(<h4>Wants to skip : </h4>)
            for (const value of this.props.votes) {
                items.push(<li>{value}</li>)
            }
        }

        return (
            <Card>
                <Grid container alignContent="center">
                    <Grid item align="center" xs={4}>
                        <img src={ this.props.image_url } height="100%" width="100%" alt=""/>
                    </Grid>
                    <Grid item align="center" xs={8} className="dark-component">
                        <Typography component="h5" variant="h5" style={{margin: '1em 0 0.2em 0'}}>
                            {this.props.title}
                        </Typography>
                        <Typography variant="subtitle1">
                            {this.props.artist}
                        </Typography>
                        <div>
                            <IconButton onClick={this.props.is_playing ? this.pauseSong : this.playSong}>
                                {this.props.is_playing ? <PauseIcon/> : <PlayArrowIcon/>}
                            </IconButton>
                            <IconButton onClick={this.skipSong}>
                                {this.props.votes === undefined ? 0 : this.props.votes.length} / {this.props.votes_required}
                                <SkipNextIcon/>
                            </IconButton>
                        </div>
                        {items}
                    </Grid>
                </Grid>
                <LinearProgress variant="determinate" value={songProgress}/>
            </Card>
            )
    }
}