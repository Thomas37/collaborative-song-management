import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import ButtonGroup from "@material-ui/core/ButtonGroup";
import TextField from "@material-ui/core/TextField";
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import SearchedSong from "./SearchedSong";

export default class AddSongPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: "",
            songs: [],
        }
        this._handleSearchTextFieldChange = this._handleSearchTextFieldChange.bind(this)
        this.searchSong = this.searchSong.bind(this)
        this.back = this.back.bind(this)
    }

    searchSong() {
        fetch('/spotify/search-song?search=' + this.state.searchString).then((res) => {
            if (!res.ok) {
                return {}
            }
            else {
                return res.json()
            }
        }).then((data) => {
            this.setState({songs: data})
        })
    }

    _handleSearchTextFieldChange(e) {
        this.setState({
            searchString: e.target.value,
        })
    }

    back() {
        this.props.history.goBack()
    }

    render() {

        const items = []

        if (this.state.songs !== undefined && this.state.songs.length > 0) {
            for (const value of this.state.songs) {
                items.push(<SearchedSong album_cover={value.album_cover} name={value.name} artist_string={value.artist_string} uri={value.uri}/>)
            }
        }


        return (
            <Grid container>
                <Grid item xs={12} align="center">
                    <Typography variant="h4" component="h4">
                        Search a song
                    </Typography>
                </Grid>
                <Grid item xs={12} align="center">
                    <TextField
                        error= { this.state.error }
                        label="Code"
                        placeholder="Enter a title"
                        value={ this.state.searchString }
                        helperText={ this.state.error }
                        variant="outlined"
                        onChange={ this._handleSearchTextFieldChange }
                        fullWidth="100%"
                    />
                    <ButtonGroup variant="contained" color="primary" style={{margin: "1em"}}>
                        <Button color="primary" variant="contained" onClick={this.searchSong}>
                            <SearchRoundedIcon/>
                            Search
                        </Button>
                        <Button color="secondary" variant="contained" onClick={this.back}>
                            Back
                        </Button>
                    </ButtonGroup>
                </Grid>

                <Grid item xs={12} align="center">
                    {items}
                </Grid>
            </Grid>
            )
    }
}