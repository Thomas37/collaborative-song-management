import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import MusicPlayer from "./MusicPlayer";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import teal from "@material-ui/core/colors"
import {LinearProgress} from "@material-ui/core";
import Card from "@material-ui/core/Card";

export default class RoomPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guestCanPause: false,
            votesToSkip: 2,
            isHost: false,
            spotifyAuthenticated: false,
            song: {},
            queue: []
        }
        this.roomCode = this.props.match.params.roomCode;
        this.leaveButtonPressed = this.leaveButtonPressed.bind(this)
        this.authenticateSpotify = this.authenticateSpotify.bind(this)
        this.getCurrentSong = this.getCurrentSong.bind(this)
        this.getRoomDetails()
        this.getCurrentSong()
    }

    componentDidMount() {
        this.interval = setInterval(this.getCurrentSong, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    getCurrentSong() {
        fetch('/spotify/current-song').then((res) => {
            if (!res.ok) {
                return {}
            }
            else {
                return res.json()
            }
        }).then((data) => {
            this.setState({song: data})
        })

        fetch('/spotify/get-queue').then((res) => {
            if (!res.ok) {
                return {}
            }
            else {
                return res.json()
            }
        }).then((data) => {
            this.setState({queue: data})
        })
    }

    renderSettingsButton() {
        return (
                <Button variant="contained" color="primary" onClick={()=> this.props.history.push("/update?guestCanPause=" + this.state.guestCanPause.toString() + "&votesToSkip=" + this.state.votesToSkip)}>
                    Settings
                </Button>
        )
    }

    getRoomDetails(){

        fetch('/api/get-room?code=' + this.roomCode)
            .then((response) => {
                if (!response.ok) {
                    this.props.leaveRoomCallback();
                    this.props.history.push("/")
                }

                return response.json()
            })
            .then((data) => {
                this.setState({
                    votesToSkip: data.votes_to_skip,
                    guestCanPause: data.guest_can_pause,
                    isHost: data.is_host,
                })
                if (this.state.isHost) {
                    this.authenticateSpotify()
                }
            })
    }

    authenticateSpotify() {

        fetch('/spotify/is-authenticated').then((res) => res.json())
            .then((data) => {
                this.setState({
                    spotifyAuthenticated: data.status
                })
                if (!data.status) {
                    fetch('/spotify/get-auth-url').then((res) => res.json())
                        .then((data) => {
                            console.log(data.url)
                            window.location.replace(data.url)
                        })
                }
            })
    }

    leaveButtonPressed() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                code: this.roomCode,
            })
        }

        fetch('/api/leave-room', requestOptions)
            .then((_response) => {
                this.props.leaveRoomCallback();
                this.props.history.push("/")
            })
    }

    render() {

        const items = []

        if (this.state.queue !== undefined && this.state.queue.length > 0) {
            for (const value of this.state.queue) {
                items.push(
                    <Card>
                        <Grid container alignContent="center">
                            <Grid item align="center" xs={4}>
                                <img src={ value.song_picture_url } height="100%" width="100%" alt=""/>
                            </Grid>
                            <Grid item align="center" xs={8} className="dark-component">
                                <Typography component="h5" variant="h5" style={{margin: '1em 0 0.2em 0'}}>
                                    {value.song_title}
                                </Typography>
                                <Typography variant="subtitle1">
                                    {value.song_artist}
                                </Typography>
                                <Typography variant="subtitle2">
                                    Added by {value.pseudo}
                                </Typography>
                            </Grid>
                        </Grid>
                        <LinearProgress variant="determinate" value={100}/>
                    </Card>
                )
            }
        }

        return (
            <Grid container spacing={1}>
                <Grid item xs={12} align="center">
                    <Typography variant="h4" component="h4">
                        Code : {this.roomCode}
                    </Typography>
                </Grid>

                <Grid item xs={12} align="center">
                    <MusicPlayer {...this.state.song}/>
                </Grid>


                <Grid item xs={12} align="center">
                    <ButtonGroup variant="contained" color="primary" >
                        { this.state.isHost ? this.renderSettingsButton() : null }

                        <Button variant="contained" to="/add-song" component={Link} style={{backgroundColor: "teal"}}>
                            Add a song
                        </Button>
                        <Button color="secondary" variant="contained" to="/" component={Link} onClick={this.leaveButtonPressed}>
                            Leave
                        </Button>
                    </ButtonGroup>
                </Grid>

                <Grid item xs={12} align="center">
                    {items}
                </Grid>
            </Grid>
            )
    }
}