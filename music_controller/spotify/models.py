from django.db import models

from api.models import Room


class SpotifyToken(models.Model):
    user = models.CharField(max_length=50, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    refresh_token = models.CharField(max_length=150)
    access_token = models.CharField(max_length=150)
    expires_in = models.DateTimeField()
    token_type = models.CharField(max_length=50)


class Vote(models.Model):
    user = models.CharField(max_length=50, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    song_id = models.CharField(max_length=50)
    pseudo = models.CharField(max_length=50, default="anonymous")
    room = models.ForeignKey(Room, on_delete=models.CASCADE)


class NextTracks(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    song_title = models.CharField(max_length=50)
    song_artist = models.CharField(max_length=100)
    song_picture_url = models.CharField(max_length=150)
    pseudo = models.CharField(max_length=50, default="anonymous")
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
