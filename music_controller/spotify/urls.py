from django.urls import path
from .views import AuthURL, spotify_callback, IsAuthenticated, CurrentSongView, PauseSongView, PlaySongView, \
    SkipSongView, SearchSongView, AddSongView, GetQueueView

urlpatterns = [
    path('get-auth-url', AuthURL.as_view()),
    path('redirect', spotify_callback),
    path('is-authenticated', IsAuthenticated.as_view()),
    path('current-song', CurrentSongView.as_view()),
    path('search-song', SearchSongView.as_view()),
    path('add-song', AddSongView.as_view()),
    path('get-queue', GetQueueView.as_view()),
    path('play', PlaySongView.as_view()),
    path('pause', PauseSongView.as_view()),
    path('skip', SkipSongView.as_view()),
]