from .credentials import HOST

from django.shortcuts import render, redirect
from .credentials import REDIRECT_URI, CLIENT_SECRET, CLIENT_ID
from rest_framework.views import APIView
from requests import Request, post
from rest_framework import status
from rest_framework.response import Response

from .models import Vote, NextTracks
from .serializers import NextTrackSerializer
from .utils import update_or_create_user_tokens, is_spotify_authenticated, get_user_tokens, \
    execute_spotify_request, play_song, pause_song, skip_song  # , execute_spotify_api_call
from api.models import Room


# from ..api.models import Room


class AuthURL(APIView):
    def get(self, request, fornat=None):
        scopes = 'user-read-playback-state user-modify-playback-state user-read-currently-playing'

        url = Request('GET', 'https://accounts.spotify.com/authorize', params={
            'scope': scopes,
            'response_type': 'code',
            'redirect_uri': REDIRECT_URI,
            'client_id': CLIENT_ID
        }).prepare().url

        return Response({'url': url}, status=status.HTTP_200_OK)


def spotify_callback(request, format=None):
    code = request.GET.get('code')
    error = request.GET.get('error')

    response = post('https://accounts.spotify.com/api/token', data={
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': REDIRECT_URI,
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET
    }).json()

    access_token = response.get('access_token')
    token_type = response.get('token_type')
    refresh_token = response.get('refresh_token')
    expires_in = response.get('expires_in')
    error = response.get('error')

    if not request.session.exists(request.session.session_key):
        request.session.create()

    update_or_create_user_tokens(
        request.session.session_key, access_token, token_type, expires_in, refresh_token)

    return redirect(HOST)


class IsAuthenticated(APIView):
    def get(self, request, format=None):
        is_authenticated = is_spotify_authenticated(
            self.request.session.session_key)
        return Response({'status': is_authenticated}, status=status.HTTP_200_OK)


class CurrentSongView(APIView):
    def get(self, request, format=None):
        room_code = self.request.session.get('room_code')
        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        host = room.host
        endpoint = "me/player/currently-playing"
        response = execute_spotify_request(host, endpoint)

        if 'error' in response or 'item' not in response:
            return Response({}, status=status.HTTP_204_NO_CONTENT)

        item = response.get('item')
        duration = item.get('duration_ms')
        progress = response.get('progress_ms')
        album_cover = item.get('album').get('images')[0].get('url')
        is_playing = response.get('is_playing')
        song_id = item.get('id')
        artist_string = ""

        for i, artist in enumerate(item.get('artists')):
            if i > 0:
                artist_string += ", "
            name = artist.get('name')
            artist_string += name

        votes = []
        votes_query_set = Vote.objects.filter(room=room)
        if votes_query_set.exists():
            for vote in votes_query_set.all():
                votes.append(vote.pseudo)

        song = {
            'title': item.get('name'),
            'artist': artist_string,
            'duration': duration,
            'time': progress,
            'image_url': album_cover,
            'is_playing': is_playing,
            'votes': votes,
            'votes_required': room.votes_to_skip,
            'id': song_id
        }

        self.update_room_song(room, song_id)

        return Response(song, status=status.HTTP_200_OK)

    def update_room_song(self, room, song_id):
        current_song = room.current_song

        if current_song != song_id:
            room.current_song = song_id
            room.save(update_fields=['current_song'])
            Vote.objects.filter(room=room).delete()
            next_tracks = NextTracks.objects.filter(room=room)
            if next_tracks.exists():
                next_tracks[0].delete()


class SearchSongView(APIView):

    def get(self, request, format=None):
        search_string = request.GET.get('search')
        room_code = self.request.session.get('room_code')

        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        host = room.host
        endpoint = "search?q=" + search_string + "&type=track"
        is_spotify_authenticated(host)
        response = execute_spotify_request(host, endpoint)

        if 'error' in response or 'tracks' not in response:
            return Response({}, status=status.HTTP_204_NO_CONTENT)

        songs = []

        for item in response["tracks"]["items"]:
            duration = item.get('duration_ms')
            name = item.get('name')
            album_cover = item.get('album').get('images')[0].get('url')
            uri = item.get('uri')

            artist_string = ""
            for i, artist in enumerate(item.get('artists')):
                if i > 0:
                    artist_string += ", "
                artist_name = artist.get('name')
                artist_string += artist_name

            songs.append({
                'duration': duration,
                'name': name,
                'album_cover': album_cover,
                'uri': uri,
                'artist_string': artist_string,
            })

        return Response(songs, status=status.HTTP_200_OK)


class AddSongView(APIView):

    def post(self, request, format=None):
        uri = request.GET.get('uri')
        room_code = self.request.session.get('room_code')
        title = request.GET.get('title')
        artist = request.GET.get('artist')
        picture = request.GET.get('picture')

        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        host = room.host

        endpoint = "me/player/queue?uri=" + uri
        is_spotify_authenticated(host)
        response = execute_spotify_request(host, endpoint, post_=True)

        pseudo = self.request.session.get('pseudo')
        if pseudo is None:
            pseudo = "Administrator"

        next_track = NextTracks(song_title=title, song_artist=artist, song_picture_url=picture, pseudo=pseudo, room=room)
        next_track.save()
        print("saved")

        return Response(response, status=status.HTTP_204_NO_CONTENT)


class GetQueueView(APIView):

    def get(self, request, format=None):
        room_code = self.request.session.get('room_code')

        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        host = room.host

        next_tracks = NextTracks.objects.filter(room=room)
        res = []
        for next_track in next_tracks:
            res.append(NextTrackSerializer(next_track).data)

        return Response(res, status=status.HTTP_200_OK)


class PlaySongView(APIView):
    def put(self, request, format=None):
        room_code = self.request.session.get('room_code')
        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        if self.request.session.session_key == room.host or room.guest_can_pause:
            play_song(room.host)
            host = room.host
            endpoint = "me/player/currently-playing"
            response = execute_spotify_request(host, endpoint)
            return Response({}, status=status.HTTP_204_NO_CONTENT)
        return Response({}, status=status.HTTP_403_FORBIDDEN)


class PauseSongView(APIView):
    def put(self, request, format=None):
        room_code = self.request.session.get('room_code')
        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        if self.request.session.session_key == room.host or room.guest_can_pause:
            pause_song(room.host)
            return Response({}, status=status.HTTP_204_NO_CONTENT)
        return Response({}, status=status.HTTP_403_FORBIDDEN)


class SkipSongView(APIView):
    def post(self, request, format=None):
        room_code = self.request.session.get('room_code')
        room = Room.objects.filter(code=room_code)
        if room.exists():
            room = room[0]
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        votes = Vote.objects.filter(user=self.request.session.session_key, room=room)

        if votes.exists():
            votes[0].delete()
        else:
            vote = Vote(user=self.request.session.session_key,
                        room=room, song_id=room.current_song,
                        pseudo="anonymous" if self.request.session.get('pseudo') is None else self.request.session.get(
                            'pseudo'))
            vote.save()

        votes = Vote.objects.filter(room=room, song_id=room.current_song)
        votes_needed = room.votes_to_skip

        if self.request.session.session_key == room.host or len(votes) >= votes_needed:
            skip_song(room.host)

        return Response({}, status=status.HTTP_204_NO_CONTENT)
