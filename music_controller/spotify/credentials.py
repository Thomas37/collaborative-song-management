import os

CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
HOST = os.getenv('HOST')
REDIRECT_URI = os.getenv('HOST') + "/spotify/redirect" if HOST is not None else ""
