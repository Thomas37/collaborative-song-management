from rest_framework import serializers
from .models import NextTracks


class NextTrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = NextTracks
        fields = ("song_title", "song_artist", "song_picture_url", "pseudo")
